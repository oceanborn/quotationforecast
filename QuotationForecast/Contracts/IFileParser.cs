﻿using System.Collections.Generic;
using QuotationForecast.Core.Models;

namespace QuotationForecast.Contracts
{
    public interface IFileParser
    {
        IEnumerable<string> AcceptableFileTypes { get; }

        IEnumerable<TimelineItem> ParseFile(string file);
    }
}

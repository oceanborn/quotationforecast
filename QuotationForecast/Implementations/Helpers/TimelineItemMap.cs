﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration;
using QuotationForecast.Core.Models;

namespace QuotationForecast.Implementations.Helpers
{
    public class TimelineItemMap : CsvClassMap<TimelineItem>
    {
        public override void CreateMap()
        {
            Map(m => m.Date).Name("Date");
            Map(m => m.GbpToUsd).Name("GBP/USD");
            Map(m => m.EurToUsd).Name("EUR/USD");
            Map(m => m.ChfToUsd).Name("CHF/USD");
            Map(m => m.CadToUsd).Name("CAD/USD");
            Map(m => m.JpyToUsd).Name("JPY/USD");
            Map(m => m.MxnToUsd).Name("MXN/USD");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using QuotationForecast.Contracts;
using QuotationForecast.Core.Models;
using QuotationForecast.Implementations.Helpers;

namespace QuotationForecast.Implementations
{
    public class CsvFileParser : IFileParser
    {
        public IEnumerable<string> AcceptableFileTypes { get { return new[] {"csv"}; } }

        public IEnumerable<TimelineItem> ParseFile(string file)
        {
            using (var csv = new CsvReader(new StreamReader(File.OpenRead(file))))
            {
                csv.Configuration.RegisterClassMap<TimelineItemMap>();
                csv.Configuration.TrimFields = true;
                csv.Configuration.Delimiter = ";";

                var records = csv.GetRecords<TimelineItem>().ToList();
                return records;
            }
        }
    }
}

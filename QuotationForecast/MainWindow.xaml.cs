﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls.DataVisualization.Charting;
using LABARABOKON;
using Microsoft.Win32;
using QuotationForecast.Core.Models;
using QuotationForecast.Implementations;

namespace QuotationForecast
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Boreliv _boreliv;

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void SelectFileButtonClick(object sender, RoutedEventArgs e)
        {
            var openDialog = new OpenFileDialog();
            openDialog.Multiselect = false;
            openDialog.Filter = "Csv File (*.csv)|*.csv";

            if (!openDialog.ShowDialog().Value)
            {
                return;
            }

            var fileName = openDialog.FileName;

            var parser = new CsvFileParser();
            var records = parser.ParseFile(fileName);

            DataGrig.DataContext = records;

            Chart.BeginInit();
            Chart.EndInit();

            var gpbSeries = Chart.Series[0] as LineSeries;
            gpbSeries.ItemsSource = records.Select(x => new
                                                            {
                                                                Date = x.Date,
                                                                Value = x.GbpToUsd
                                                            });

            var eurSeries = Chart.Series[1] as LineSeries;
            eurSeries.ItemsSource = records.Select(x => new
                                                            {
                                                                Date = x.Date,
                                                                Value = x.EurToUsd
                                                            });

            var chfSeries = Chart.Series[2] as LineSeries;
            chfSeries.ItemsSource = records.Select(x => new
                                                            {
                                                                Date = x.Date,
                                                                Value = x.ChfToUsd
                                                            });

            var cadSeries = Chart.Series[3] as LineSeries;
            cadSeries.ItemsSource = records.Select(x => new
                                                            {
                                                                Date = x.Date,
                                                                Value = x.CadToUsd
                                                            });

            var jpySeries = Chart.Series[4] as LineSeries;
            jpySeries.ItemsSource = records.Select(x => new
                                                            {
                                                                Date = x.Date,
                                                                Value = x.JpyToUsd
                                                            });

            var mxnSeries = Chart.Series[5] as LineSeries;
            mxnSeries.ItemsSource = records.Select(x => new
                                                            {
                                                                Date = x.Date,
                                                                Value = x.MxnToUsd
                                                            });

            _boreliv = new Boreliv(records);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var openDialog = new OpenFileDialog();
            openDialog.Multiselect = false;
            openDialog.Filter = "Csv File (*.csv)|*.csv";

            if (!openDialog.ShowDialog().Value)
            {
                return;
            }

            var fileName = openDialog.FileName;

            var parser = new CsvFileParser();
            var records = parser.ParseFile(fileName);


           
            var list = ((IEnumerable<TimelineItem>) records).ToList();
            EpsilonBox.Text = _boreliv.Eps(list).ToString();
        }
    }
}

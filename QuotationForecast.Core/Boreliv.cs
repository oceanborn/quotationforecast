﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab_2.Objects;
using Lab_2.Methods;
using QuotationForecast.Core.Models;

namespace LABARABOKON
{
    public class Boreliv
    {
        private List<Currency> currencies;
        private int N;
        private int t;
        private List<NormalGauss> NG;

        public Boreliv(IEnumerable<TimelineItem> items)
        {
            Create(Convert(items), DateTime.Now);
        }

        public Vector Convert(TimelineItem item)
        {
            var list = item.ToModel();
            var vector = new Vector(list.Count());
            for(var i = 0; i < list.Count(); i++)
            {
                vector[i + 1] = list.ElementAt(i);
            }

            return vector;
        }

        public List<Vector> Convert(IEnumerable<TimelineItem> items)
        {
            return items.Select(Convert).ToList();
        }

        public class Currency
        {
            public Currency(Vector curv, Vector nextv, DateTime dt)
            {
                date = dt;
                cur = curv;
                MaxNext = Max(curv, nextv);
            }

            public Currency(Vector curv, DateTime dt)
            {
                date = dt;
                cur = curv;
                MaxNext = -1;
            }

            public DateTime date;
            public Vector cur;
            public int MaxNext;

            private int Max(Vector v1, Vector v2)
            {
                var v = v2 - v1;
                double max = v[1];
                int i = 1;
                for (int j = 2; j < v.N; j++)
                {
                    if (v[j] > max)
                    {
                        max = v[j];
                        i = j;
                    }
                }
                return i;
            }

        }

        private List<Currency> CreateCurren(List<Vector> X, DateTime bg)
        {
            var y = new List<Currency>();
            for (int i = 0; i < X.Count - 1; i++)
            {
                y.Add(new Currency(X[i], X[i + 1], bg.AddDays(i)));
            }
            return y;
        }

        private void Create(List<Vector> X, DateTime bg)
        {
            N = 6;
            currencies = CreateCurren(X, bg);
            NG = new List<NormalGauss>();
            for (int i = 1; i <= N; i++)
            {
                var vic = (from curr in currencies where curr.MaxNext == i select curr.cur).ToList();
                NG.Add(new NormalGauss(vic));
            }

        }

        public int Vic(Currency x)
        {
            double p = 0;
            double Max = Risk(1, x.cur);
            int iMax = 1;
            for (int i = 2; i <= N; i++)
            {
                var cMax = Risk(i, x.cur);
                if (cMax > Max)
                {
                    Max = cMax;
                    iMax = i;
                }
            }
            return iMax;
        }

        public Double Eps(IEnumerable<TimelineItem> items)
        {
            var X = CreateCurren(Convert(items), DateTime.Now);
            int wrong = 0;
            foreach (var currency in X)
            {
                if (Vic(currency) != currency.MaxNext)
                    wrong++;
            }
            double eps = wrong/(X.Count*1.0);
            return eps > 0.8 ? 1 - eps : eps;
        }

        private double Risk(int k, Vector x)
        {
            k--;
            return (NG[k].P(x)*NG[k].cK());
        }
    }

}

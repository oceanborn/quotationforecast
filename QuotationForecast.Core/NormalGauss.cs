﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Lab_2.Objects;
using Lab_2.Methods;

namespace LABARABOKON
{
    class NormalGauss
    {
        private Vector Mat;
      
        private double Disp;
        private int K;
        private Func<double, double, double> f;

 
        public NormalGauss(List<Vector> X )
        {
            if (!X.Any())
            {
                K = 0;
                f = (x, m) => 0;
                Mat = new Vector(0);
                Disp = 0;
                return;
            }

            Create(X);
        }
        private void Create(List<Vector> x1)
        {
            K = x1.Count;
            Mat=CMat(x1);
            Disp = CDisp(CKovar(x1, Mat));
            Disp = Disp*Math.Pow(10, 25);
            f = (x, m) => 1 / (Math.Sqrt(2 * Math.PI * Disp)) * Math.Exp(-Math.Pow(x - m, 2) / (2 * Disp));
        }

        private Double CDisp(Matrix E)
        {
            return Methods.Det(E);
        }

        private Matrix CKovar(List<Vector> x,Vector m )
        {
            Matrix ans=new Matrix(m.N);
            foreach (var vector in x)
            {
                ans += Vector.VecDob(vector - m, vector - m);
            }
            ans = ans/x.Count;
            return ans;
        }
        private Vector CMat(List<Vector> x )
        {
            var n = x.Count;
            var y=new Vector(x[0].N);
            y = x.Aggregate(y, (current, cur) => current + cur);
            y /= n;
                return y;
        }

        public double P(Vector p )
        {
            var ans=new List<double>();
           
            for(int i=1;i<=p.N;i++)
            {
                if(Mat.N==0)
                {
                    ans.Add(0);
                }
                else
                {
                       ans.Add(f(p[i],Mat[i]));
                }
             
            }
            return ans.Sum()/ans.Count;
        }
        public int cK()
        {
            return K;
        }

    }
}

﻿using System;
using Lab_2.Objects;

namespace Lab_2.Methods
{
    class Methods
    {
        //Parameter for relaxation method
        //public static readonly Double w = 0.2;

        private static bool ChekSymetric(Matrix A)
        {
            for (var i = 1; i <= A.N; i++)

                for (var j = 1; j < i; j++)
                    if (A[i, j] != A[j, i])
                        return false;

            return true;

        }

        public static Matrix InverseMatrixGaus(Matrix A)
        {
            var Answer=new Matrix(A.N);
            for (int r = 1; r <= A.N; r++)
            {
                var A1 = new Matrix(A.N);
                var b1 = new Vector(A.N);
                b1[r] = 1;
                for (int i = 1; i <= A.N; i++)
                {
                    for (int j = 1; j <= A.N; j++)
                    {
                        A1[i, j] = A[i, j];
                    }
                }


                for (var i = 1; i < A.N; i++)
                {
                    var c = A1[i, i];
                    for (var j = 1; j <= A.N; j++)
                    {
                        A1[i, j] = A1[i, j]/c;
                    }
                    b1[i] = b1[i]/c;

                    for (int k = i + 1; k <= A.N; k++)
                    {
                        var p = A1[i + 1, i];
                        for (int j = 1; j <= A.N; j++)
                        {
                            A1[k, j] = A1[k, j] - p*A1[i, j];
                        }
                        b1[k] = b1[k] - b1[i]*p;
                    }
                }
                b1[A.N] = b1[A.N]/A1[A.N, A.N];
                A1[A.N, A.N] = 1;
                Vector x = new Vector(A.N);
                x = b1;
                for (int i = A.N; i >= 1; i--)
                {
                    for (int j = i + 1; j <= A.N; j++)
                    {
                        x[i] = x[i] - x[j]*A1[i, j];
                    }
                    x[i] = x[i]/A1[i, i];
                }
                for (int i = 1; i <= A.N; i++)
                {
                    Answer[i, r] = x[i];
                }
            }
            return Answer;
        }

        public static Solution Gauss(Matrix A,Vector b)
        {
            var A1=new Matrix(A.N);
            var b1 = new Vector(A.N);
            for (int i = 1; i <= A.N; i++)
            {
                for(int j=1;j<=A.N;j++)
                {
                    A1[i, j] = A[i, j];
                }
                b1[i] = b[i];
            }

         
            for(var i=1;i<A.N;i++)
            {
                var c = A1[i, i];
                for(var j=1;j<=A.N;j++)
                {
                    A1[i, j] = A1[i, j]/c;
                }
                b1[i] = b1[i]/c;

                for(int k=i+1;k<=A.N;k++)
                {
                    var p = A1[i + 1, i];
                    for(int j=1;j<=A.N;j++)
                    {
                        A1[k, j] = A1[k, j] - p*A1[i, j];
                    }
                    b1[k] = b1[k] - b1[i]*p;
                }
            }
            b1[A.N] = b1[A.N]/A1[A.N, A.N];
            A1[A.N, A.N] = 1;
            Vector x=new Vector(A.N);
            x = b1;
            for(int i =A.N;i>=1;i--)
            {
                for(int j=i+1;j<=A.N;j++)
                {
                    x[i] = x[i] - x[j]*A1[i, j];
                }
                x[i] = x[i]/A1[i, i];
            }

            x.SetVectorType(VECTOR_TYPE.Column);
            var solution = new Solution(A.N);
            //Adding input matrix to Solution object.
            solution.A = A;

            //Adding input value-vector to Solution object.
            solution.b = b;
            solution.detA = 5456;
            //Adding vector-solution to Solution object.
            solution.x = x;
         
            //Adding vector-deviation to Solution object.
            solution.r = A * x - b;
        
            //Adding inversed matrix to Solution object.
            solution.invA = InverseMatrixGaus(A);

            //Adding deviation matrix to Solution object.
            solution.devA = solution.invA * A;

            //Adding condition number to Solution object.
            solution.condA = InfinityMatrixNorm(A) * InfinityMatrixNorm(solution.invA);
            return solution;
        }

        public static double Det(Matrix A)
        {
            var A1 = new Matrix(A.N);
       
            for (int i = 1; i <= A.N; i++)
            {
                for (int j = 1; j <= A.N; j++)
                {
                    A1[i, j] = A[i, j];
                }
      
            }


            for (var i = 1; i < A.N; i++)
            {
                var c = A1[i, i];
                for (var j = 1; j <= A.N; j++)
                {
                    A1[i, j] = A1[i, j] / c;
                }
   

                for (int k = i + 1; k <= A.N; k++)
                {
                    var p = A1[i + 1, i];
                    for (int j = 1; j <= A.N; j++)
                    {
                        A1[k, j] = A1[k, j] - p * A1[i, j];
                    }
     
                }
            }
     
            A1[A.N, A.N] = 1;
            double det = 1;
            for(int i=1;i<=A.N;i++)
            {
                det *= A[i, i];
            }
            return det;
        }
        private static bool Cheknull(Matrix A)
        {
            for (var i = 1; i <= A.N; i++)
                    if (A[i, i] == 0)
                        return false;

            return true;

        }
            public static Solution SquareRootMethod(Matrix A, Vector b)
        {
            var solution = new Solution(A.N);

            var G = new Matrix(A.N);
            var D = new Matrix(A.N);
            Double quadraticSum;

         
            if (!ChekSymetric(A))
            {
                var ex = new Exception { HelpLink = "Check your input. Convergence's not all right.Matrix is not symetric =( \n" };
                throw ex;
            }

            if (A[1,1]==0)
            {
                var ex = new Exception { HelpLink = "Check your input. Convergence's not all right.First element on diagonal is null=( \n" };
                throw ex;
            }
            G[1, 1] = Math.Pow(Math.Sign(A[1, 1])*A[1, 1], 0.5);
            D[1, 1] = Math.Sign(A[1, 1]);
            for (var i = 2; i <= G.N; i++)
            {
                G[i, 1] = A[i, 1] / (G[1, 1] * D[1, 1]);
            }
   
            for (var j = 2; j <= G.N; j++)
            {
                //calculating diagonal element
                quadraticSum = 0;
                for (int k = 1; k <= j - 1; k++)
                {
                    quadraticSum +=Math.Pow(G[j, k], 2.0)*D[k, k] ;
                }

                D[j, j] = Math.Sign(A[j, j] - quadraticSum);
                G[j, j] = Math.Pow((A[j, j] - quadraticSum) * D[j, j], 0.5); 

                //calculating under diagonal elements
                for (int i = j+1; i <= G.N; i++)
                {
                    quadraticSum = 0;
                    for (var k = 1; k <= i - 1; k++)
                    {
                        quadraticSum += G[i, k] * D[k, k] * G[j, k];
                    }
                    if (G[j, j]!=0)
                    G[i, j] = (A[i, j] - quadraticSum) / (G[j, j] * D[j, j]);
                    else
                    {
                        G[j, j] = 0;
                    }
                }
            }            

            Double detA = 1;
            for (var i = 1; i <= G.N; i++)
            {
                detA *= Math.Pow(G[i, i], 2.0) * D[i, i];
            }

            //Adding determinant to Solution object.
            solution.detA = detA;          
            
            //Reverse course

                //Reversal of Gaussian method from top to down
            var B = G * D;
            var y = new Vector(G.N);

            y[1] = b[1] / B[1, 1];

            for (int i = 2; i <= y.N; i++)
            {
                quadraticSum = 0;
                for (int k = 1; k<= i-1; k++)
                {
                    quadraticSum += B[i, k] * y[k];
                }
                y[i] = (b[i] - quadraticSum) / B[i, i];
            }

                //Reversal of Gaussian method from down to top
            var x = new Vector(G.N);

            x[x.N] = y[y.N] / G[G.N, G.N];

            for (var i = x.N-1; i >= 1; i--)
            {
                quadraticSum = 0;
                for (var k = i + 1; k <= x.N; k++)
                {
                    quadraticSum += G[k, i] * x[k];
                }
                x[i] = (y[i] - quadraticSum) / G[i, i];
            }
            x.SetVectorType(VECTOR_TYPE.Column);

            //Adding input matrix to Solution object.
            solution.A = A;

            //Adding input value-vector to Solution object.
            solution.b = b;

            //Adding vector-solution to Solution object.
            solution.x = x;

            //Adding vector-deviation to Solution object.
            solution.r = A*x - b;

            //Adding inversed matrix to Solution object.
            solution.invA = InverseMatrix(B, G);

            //Adding deviation matrix to Solution object.
            solution.devA = solution.invA * A;

            //Adding condition number to Solution object.
            solution.condA = InfinityMatrixNorm(A) * InfinityMatrixNorm(solution.invA);


            return solution;
        }

        /// <summary>
        /// Calculating inversed matrix to matrix A by using results of SquareRoot Method.
        /// </summary>
        /// <param name="B">B = A * S(-1), where S is right triangle matrix.</param>
        /// <param name="S">Right triangle matrix.</param>
        /// <returns>Inversed Matrix</returns>
        public static Matrix InverseMatrix(Matrix B, Matrix S)
        {
            var inverseMatrix = new Matrix(B.N);
            var eMatrix = new Vector[B.N];
            for (var i = 0; i < B.N; i++)
            {
                eMatrix[i] = new Vector(B.N);
                eMatrix[i][i+1] = 1;
            }

            var y = new Vector(B.N);            
            Double quadraticSum;

            for (int p = 0; p < B.N; p++)
            {
                y.ToNull();                

                //Reversal of Gaussian method from top to down
                y[1] = eMatrix[p][1] / B[1, 1];

                for (var i = 2; i <= y.N; i++)
                {
                    quadraticSum = 0;
                    for (int k = 1; k <= i - 1; k++)
                    {
                        quadraticSum += B[i, k] * y[k];
                    }
                    y[i] = (eMatrix[p][i] - quadraticSum) / B[i, i];
                }


                inverseMatrix[inverseMatrix.N, p+1] = y[y.N] / S[S.N, S.N];

                for (var i = inverseMatrix.N - 1; i >= 1; i--)
                {
                    quadraticSum = 0;
                    for (var k = i + 1; k <= inverseMatrix.N; k++)
                    {
                        quadraticSum += S[k, i] * inverseMatrix[k, p+1];
                    }
                    inverseMatrix[i, p+1] = (y[i] - quadraticSum) / S[i, i];
                }                
            }
            return inverseMatrix;

        }

        /// <summary>
        /// Calculating matrix norm_1.
        /// </summary>
        /// <param name="A"></param>
        /// <returns>Matrix norm_1.</returns>
 

        /// <summary>
        /// Calculating matrix norm_infinity.
        /// </summary>
        /// <param name="A"></param>
        /// <returns>Matrix norm_infinity.</returns>
        public static Double InfinityMatrixNorm(Matrix A)
        {
            Double maxRowSum = 0;
            for (var i = 1; i <= A.N; i++)
            {
                Double rowSum = 0;
                for (var j = 1; j <= A.N; j++)
                {
                    rowSum += A[i, j] * Math.Sign(A[i, j]);
                }
                maxRowSum = Math.Max(maxRowSum, rowSum);
            }
            return maxRowSum;
        }

        private static bool converge(Vector xk, Vector xkp,Double eps)
        {
            for (var i = 1; i <= xk.N; i++)
            {
                if (Math.Abs(xk[i] - xkp[i]) > eps)
                {
                   return false; 
                }
            }
            return true;
        }

 private static bool conv (Matrix A)
{
for(var i = 1; i <= A.N; i++)
{
    double s = 0;
    for (var j = 1; j <= A.N; j++)
        s += Math.Abs(A[i, j]);
    if (2*Math.Abs(A[i, i]) <= s)
        return false;
}
     return true;
}
  
 
    
    public static Solution Seidels_method(Matrix A, Vector b,Double e)
        {
            var solution = new Solution(A.N);    
        	var iter = 0;
        if(!conv(A)) 
        {
            var ex=new Exception {HelpLink = "Check your input. Convergence's not all right.Diagonal perevaga=( \n"};
            throw ex;
        }

        if (!Cheknull(A))
        {
            var ex = new Exception { HelpLink = "Check your input. Convergence's not all right.There are null element on diagonal=( \n" };
            throw ex;
        }
    var xp=new Vector(A.N);xp.SetVectorType(VECTOR_TYPE.Column);
    var x = new Vector(A.N); x.SetVectorType(VECTOR_TYPE.Column);

	do
		for(var i = 1; i <= A.N; i++)
		{
		    double sum = 0;
			for(var j =1; j <i; j++)
				if(i != j)
					sum =sum+ A[i,j] * xp[j];

            for (var j = i+1; j <= A.N; j++)
                    sum = sum + A[i, j] * x[j];
			xp[i] = x[i];
			x[i] = (b[i] - sum) / A[i,i];
		    iter++;
		}
	while(!converge(x, xp, e));

            solution.x = x;
            solution.A = A;
            solution.b = b;
            solution.r = A * x - b;
            solution.iter = iter;
            solution.eps = e;
            solution.Flag = 1;
            return solution;
        }

     
    }
}

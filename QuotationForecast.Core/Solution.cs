﻿using System;
using System.IO;

namespace Lab_2.Objects
{
    public class Solution
    {
        public Matrix A;
        public Vector b;
        public Vector x;
        public Vector r;
        public Double detA;
        public Matrix devA;
        public Matrix invA;
        public Double condA;
        public Vector beginX;
        public double eps;
        public int iter;
        public int Flag;
        public Solution(int N)
        {
            x = new Vector(N);
            r = new Vector(N);
            invA = new Matrix(N);
            devA = new Matrix(N);
            detA = 0;
            condA = 0;
            beginX=new Vector(N);
            iter = 0;
            eps = 0;
            Flag = 0;
        }

        public void writeSolutionToConsole()
        {
            x.WriteConsole();            
        }

        public void writeDeviationToConsole()
        {
            r.WriteConsole();
        }

        public void writeInversedMatrixToConsole()
        {
            invA.WriteConsole();
        }

        public void writeDeviationMatrixToConsole()
        {
            devA.WriteConsole();
        }

        public void writeDeterminantToConsole()
        {
            Console.WriteLine(detA);
        }

        public void writeConditionNumberToConsole()
        {
            Console.WriteLine(condA);
        }

        public void writeCompleteSolutionToFile(string s)
        {
            s = s.Replace("//", "/");
            TextWriter fileWriter = new StreamWriter(s);            
            fileWriter.WriteLine("Complete solution for Square Root method.");
            fileWriter.WriteLine();
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Input matrix:");
            fileWriter.WriteLine();
            this.A.WriteToStream(fileWriter);
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Input value-vector:");
            fileWriter.WriteLine();
            this.b.WriteToStream(fileWriter);
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Solution vector:");
            fileWriter.WriteLine();
            this.x.WriteToStream(fileWriter);
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Deviation vector:");
            fileWriter.WriteLine();
            this.r.WriteToStream(fileWriter);
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Determinant of input matrix:");
            fileWriter.WriteLine();
            fileWriter.WriteLine(this.detA);
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Inversed matrix:");
            fileWriter.WriteLine();
            this.invA.WriteToStream(fileWriter);
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Deviation matrix:");
            fileWriter.WriteLine();
            this.devA.WriteToStream(fileWriter);
            fileWriter.WriteLine("----------------------------------------------------------------");
            fileWriter.WriteLine("Conditional number:");
            fileWriter.WriteLine();
            fileWriter.WriteLine(this.condA);
            fileWriter.WriteLine("----------------------------------------------------------------");            
            fileWriter.Close();
        }
        public string writeCompleteSolutionToString()
        {
            if (Flag == 1) return writeCompleteSolutionForiterationToString();
            var sol = "";
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + ("Complete solution for Square Root method.");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Input matrix:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + A.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Input value-vector:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + b.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Solution vector:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + x.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Deviation vector:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + r.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Determinant of input matrix:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + (detA);
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Inversed matrix:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + invA.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Deviation matrix:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + devA.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Conditional number:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + condA;
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");

            return sol;
        }
        public string writeCompleteSolutionForiterationToString()
        {
            var sol = "";

            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + ("Complete solution for Seidel iteration  method.");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Input matrix:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + A.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Input value-vector:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + b.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("first approximation vector:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + beginX.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Epsilon:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + eps;
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Solution vector:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + x.WriteToString();
            sol = sol + '\n';
            sol = sol + ("----------------------------------------------------------------");
            sol = sol + '\n';
            sol = sol + ("Iteration number:");
            sol = sol + '\n';
            sol = sol + '\n';
            sol = sol + iter;
            sol = sol + '\n';        
            sol = sol + ("----------------------------------------------------------------");
            return sol;
        }
    }
}

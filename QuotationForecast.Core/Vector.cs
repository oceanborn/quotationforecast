﻿using System;
using System.IO;
using System.Linq;

namespace Lab_2.Objects
{
    ///<summary>
    ///Class which represents vector in n-dimensional space.
    ///</summary>

    public enum VECTOR_TYPE {Row, Column};
    public class Vector
    {
        private Double[] vector;
        public  int N;
        private VECTOR_TYPE vectorType;

        /// <summary>
        /// Constructor which creates a 0 vector in linear space with specified dimension. 
        /// </summary>
        /// <param name="dimension">Dimension of a linear space in which a vector is considered.</param>
        public Vector(int dimension)
        {
            vector = new Double[dimension];
            vectorType = VECTOR_TYPE.Row;
            this.N = dimension;
            for (int i = 0; i < this.N; i++)
            {
                vector[i] = 0;
            }
        }

        /// <summary>
        /// Constructor which creates a vector by specified coordinates.
        /// </summary>
        /// <param name="s">String which represents a sequence of coordinates.</param>
        public Vector(string s)
        {
            string[] lineOfNumbers;
            vectorType = VECTOR_TYPE.Column;
            string[] q = { "\n" };
            lineOfNumbers = s.Split(q, StringSplitOptions.RemoveEmptyEntries);
         
            N = lineOfNumbers.Count();   
            vector = new Double[N];
            for (var i = 0; i < this.N; i++)
            {
                vector[i] = Convert.ToDouble(lineOfNumbers[i]);
            }         
        }

        /// <summary>
        /// Returns a dimension of a linear space in which vector is considered.
        /// </summary>
        /// <returns>Dimension of a linear space in which vector is considered.</returns>
        public int GetDimension()
        {
            return this.N;
        }

        /// <summary>
        /// Sets the type of a vector.
        /// </summary>
        /// <param name="type">Type of a vector which will be set.</param>
        public void SetVectorType(VECTOR_TYPE type)
        {
            this.vectorType = type;
        }

        /// <summary>
        /// Gets the type of a vector.
        /// </summary>
        /// <returns>Type of a vector.</returns>
        public VECTOR_TYPE GetVectorType()
        {
            return vectorType;
        }

        /// <summary>
        /// Writes vector in console.
        /// </summary>
        public void WriteConsole()
        {
            for (var i = 0; i < this.N; i++)
            {             
                 Console.Write("{0} ", vector[i]);                
            }
            Console.WriteLine();
        }

        public void WriteToStream(TextWriter sw)
        {
            for (int i = 0; i < this.N; i++)
            {
                sw.Write("{0} ", vector[i]);
            }
            sw.WriteLine();
        }

        public string WriteToString()
        {
            var sw = "(";
            for (int i = 0; i < this.N; i++)
            {
                sw=sw+vector[i]+' ';
            }
            sw=sw+')';
            return sw;
        }

        public Double this[int i]
        {
            get { return vector[i-1]; }
            set { vector[i-1] = value; }
        }

        public void ToNull()
        {
            for (int i = 0; i < N; i++)
                vector[i] = 0;
        }

        public static Vector operator +(Vector firstVector, Vector secondVector)
        {
            if (firstVector.N != secondVector.N)
            {
                throw new System.InvalidOperationException("Unable to sum two vectors with different dimensions.");
            }
            else
            {
                Vector result = new Vector(firstVector.N);
                for (int i = 1; i <= result.N; i++)
                    result[i] = firstVector[i] + secondVector[i];
                return result;
            }
        }

        public static Vector operator -(Vector firstVector, Vector secondVector)
        {
            if (firstVector.N != secondVector.N)
            {
                throw new System.InvalidOperationException("Unable to substract two vectors with different dimensions.");
            }
            else
            {
                Vector result = new Vector(firstVector.N);
                for (int i = 1; i <= result.N; i++)
                    result[i] = firstVector[i] - secondVector[i];
                return result;
            }
        }

        public static Double operator *(Vector firstVector, Vector secondVector)
        {
            if (firstVector.N != secondVector.N)
            {
                throw new System.InvalidOperationException("Unable to multiply two vectors with different dimensions.");
            }
            else
            {
                Double result = 0;
                for (int i = 1; i <= firstVector.N; i++)
                    result += firstVector[i] * secondVector[i];
                return result;
            }
        }
        public static Vector operator /(Vector vector,double k)
        {
            Vector result = new Vector(vector.N);
            for (int i = 1; i <= result.N; i++)
            {
                result[i] =  vector[i]/k;
            }
            return result;
        }
        public static Vector operator *(Double k, Vector vector)
        {
            Vector result = new Vector(vector.N);
            for (int i = 1; i <= result.N; i++)
            {
                result[i] = k * vector[i];
            }            
            return result;
        }

        public static bool operator <(Vector vector, Double eps)
        {
            for (int i = 1; i <= vector.N; i++)
            {
                if (vector[i]*System.Math.Sign(vector[i]) > eps)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool operator >(Vector vector, Double eps)
        {
            for (int i = 1; i <= vector.N; i++)
            {
                if (vector[i] * System.Math.Sign(vector[i]) < eps)
                {
                    return false;
                }
            }
            return true;
        }

     
        public static Vector operator *(Vector vector, Matrix matrix)
        {
            if (vector.N != matrix.GetDimension() || vector.vectorType == VECTOR_TYPE.Column)
            {
                if (vector.N != matrix.GetDimension())
                    throw new System.InvalidOperationException("Unable to multiply matrix by vector with different dimension.");
                else
                    throw new System.InvalidOperationException("Unable to multiply matrix by vector-column on the left.");
            }
            else
            {
                int matrixDimension = matrix.GetDimension();
                var result = new Vector(matrixDimension);
                for (int j = 1; j <= matrixDimension; j++)
                {
                    result[j] = 0;
                    for (int i = 1; i <= matrixDimension; i++)
                    {
                        result[j] += vector[i] * matrix[i, j];
                    }
                }
                return result;
            }                            
        }

        public static Vector operator *(Matrix matrix, Vector vector)
        {
            if (vector.N != matrix.GetDimension() || vector.vectorType == VECTOR_TYPE.Row)
            {
                if (vector.N != matrix.GetDimension())
                    throw new InvalidOperationException("Unable to multiply matrix by vector with different dimension.");
                throw new InvalidOperationException("Unable to multiply matrix by vector-row on the right.");
            }
            var matrixDimension = matrix.GetDimension();
            var result = new Vector(matrixDimension);
            for (var i = 1; i <= matrixDimension; i++)
            {
                result[i] = 0;
                for (var j = 1; j <= matrixDimension; j++)
                {
                    result[i] += vector[j] * matrix[i, j];
                }
            }
            return result;
        }

        public static Matrix VecDob(Vector firstVector, Vector secondVector)
        {
            if (firstVector.N != secondVector.N)
            {
                throw new System.InvalidOperationException("Unable to multiply two vectors with different dimensions.");
            }
            else
            {
                Matrix result = new Matrix(firstVector.N);
                for (int i = 1; i <= firstVector.N; i++)
                    for (int j = 1; j <= secondVector.N;j++ )
                    {  
                        result[i , j] = firstVector[i] * secondVector[j];
                    }
                return result;
            }
        }
        public void ReadFromString(string s)
        {
   
            string[] lineOfNumbers;
            string[] q = {"\n"};
            lineOfNumbers = s.Split(q,StringSplitOptions.RemoveEmptyEntries);
            N = lineOfNumbers.Count();
            for (var i = 0; i < this.N; i++)
            {
                vector[i] = Convert.ToDouble(lineOfNumbers[i]);
            }
 
        }
        /// <summary>
        /// Reads vector from file.
        /// </summary>
        /// <param name="s">File path.</param>
        public void ReadFromFile(string s)
        {
            string input;
            string[] lineOfNumbers;
            TextReader iReader = new StreamReader(s);
            input = iReader.ReadLine();
            lineOfNumbers = input.Split(' ');            
            for (int i = 0; i < this.N; i++)
            {
                vector[i] = Convert.ToDouble(lineOfNumbers[i]);
            }
            iReader.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace QuotationForecast.Core.Models
{
    public class TimelineItem
    {
        public DateTime Date { get; set; }

        public double GbpToUsd { get; set; }

        public double EurToUsd { get; set; }

        public double ChfToUsd { get; set; }

        public double CadToUsd { get; set; }

        public double JpyToUsd { get; set; }

        public double MxnToUsd { get; set; }

        public IEnumerable<double> ToModel()
        {
            return new[]
                       {
                           GbpToUsd,
                           EurToUsd,
                           ChfToUsd,
                           CadToUsd,
                           JpyToUsd,
                           MxnToUsd
                       };
        }
    }
}

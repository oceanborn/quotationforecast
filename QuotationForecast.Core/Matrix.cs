﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab_2.Objects
{
    /// <summary>
    /// Class which represents matrix.
    /// </summary>
    public class Matrix
    {
        private readonly Double[][] matrix;
        public  int N;

        /// <summary>
        /// Constructor which creates quadratic matrix in linear space with specified dimension.
        /// </summary>
        /// <param name="n">Dimension</param>
        public Matrix(int n)
        {
            matrix = new Double[n][];
            for (var i = 0; i < n; i++)
            {
                matrix[i] = new Double[n];
                for (var j = 0; j < n; j++)
                {
                    matrix[i][j] = 0;
                }
            }
            this.N = n;   
        }



        /// <summary>
        /// Returns a dimension of a linear space in which matrix is considered.
        /// </summary>
        /// <returns>Dimension of a linear space in which matrix is considered.</returns>
        public int GetDimension()
        {
            return this.N;
        }

        /// <summary>
        /// Adds deviation to n'th column.
        /// </summary>
        /// <param name="n">Number of column.</param>
        /// <param name="deviation">Value of deviation.</param>
        /// <returns>Deviated Matrix.</returns>
        public Matrix AddDeviation(int n, Double deviation)
        {
            var devM = new Matrix(this.N);
            for (int i = 1; i <= this.N; i++)
            {
                for (int j = 1; j <= this.N; j++)
                {
                    if (j == n)
                        devM[i, j] = this[i, j] + deviation;
                    else
                        devM[i, j] = this[i, j];
                }
            }
            return devM;
        }

        /// <summary>
        /// Writes matrix in console.
        /// </summary>
        public void WriteConsole()
        {
            for (var i = 0; i < this.N; i++)
            {
                for (var j = 0; j < this.N; j++)
                {
                    Console.Write("{0} ",matrix[i][j]);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Writes matrix to file.
        /// </summary>
        public void WriteToStream(TextWriter sw)
        {
          
            for (int i = 0; i < this.N; i++)
            {
                for (var j = 0; j < this.N; j++)
                {
                    sw.Write("{0} ", matrix[i][j]);
                }
                sw.WriteLine();
            }
        }

        public string WriteToString()
        {
            var sw = "";
            for (int i = 0; i < this.N; i++)
            {
                for (var j = 0; j < this.N; j++)
                {
                    sw=sw+matrix[i][j]+' ';
                }
                sw=sw+'\n';
            }
            return sw;
        }

        public Double this[int i, int j]
        {
            get { return matrix[i-1][j-1]; }
            set { matrix[i-1][j-1] = value; }
        }

        /// <summary>
        /// Transposes current matrix.
        /// </summary>
        /// <returns>Matrix which is transposed to current matrix.</returns>
        public Matrix Transpose()
        {
            var result = new Matrix(this.N);
            for (var i = 0; i < result.N; i++)
                for (var j = 0; j < result.N; j++)
                {
                    result.matrix[i][j] = matrix[j][i];
                }
            return result;
        }

        public static Matrix operator +(Matrix firstMatrix, Matrix secondMatrix)
        {
            if (firstMatrix.N != secondMatrix.N)
            {
                throw new System.InvalidOperationException("Unable to sum two matrices with different dimensions.");
            }
            var result = new Matrix(firstMatrix.N);
            for (var i=1; i<=result.N; i++)
                for (int j = 1; j <= result.N; j++)
                {
                    result[i,j] = firstMatrix[i,j] + secondMatrix[i,j];
                }
            return result;
        }

        public static Matrix operator -(Matrix firstMatrix, Matrix secondMatrix)
        {
            if (firstMatrix.N != secondMatrix.N)
            {
                throw new System.InvalidOperationException("Unable to substract two matrices with different dimensions.");
            }
            else
            {
                Matrix result = new Matrix(firstMatrix.N);
                for (int i = 1; i <= result.N; i++)
                    for (int j = 1; j <= result.N; j++)
                    {
                        result[i,j] = firstMatrix[i,j] - secondMatrix[i,j];
                    }
                return result;
            }
        }

        public static Matrix operator *(Double k, Matrix matrix)
        {
            var result = new Matrix(matrix.N);
            for (int i=1; i<=result.N; i++)
                for (int j = 1; j <= result.N; j++)
                {
                    result[i, j] = k * matrix[i, j];
                }
            return result;
        }

        public static Matrix operator /(Matrix matrix,Double k)
        {
            var result = new Matrix(matrix.N);
            for (int i = 1; i <= result.N; i++)
                for (int j = 1; j <= result.N; j++)
                {
                    result[i, j] =  matrix[i, j]/k;
                }
            return result;
        }

        public static Matrix operator *(Matrix firstMatrix, Matrix secondMatrix)
        {
            if (firstMatrix.N != secondMatrix.N)
            {
                throw new InvalidOperationException("Unable to multiply two matrices with different dimensions.");
            }
            var result = new Matrix(firstMatrix.N);
            for (var i = 1; i <= result.N; i++)
                for (var j = 1; j <= result.N; j++)
                {
                    result[i, j] = 0;
                    for (var k = 1; k <= result.N; k++)
                    {
                        result[i, j] += firstMatrix[i, k] * secondMatrix[k, j];
                    }
                }
            return result;
        }

        public void ReadFromString(string s)
        {
            string[] q = {"\n"};
            string[] q1 = { " " };
            var iReader = s.Split(q,StringSplitOptions.RemoveEmptyEntries);
           
            N = iReader.Count(); 
            for (var i = 0; i < N; i++)
            {
                var lineOfNumbers = iReader[i].Split(q1,StringSplitOptions.RemoveEmptyEntries);
                for (var j = 0; j < this.N; j++)
                {
                    matrix[i][j] = Convert.ToDouble(lineOfNumbers[j]);
                }
            }
        }

        public void ReadFromFile(string s)
        {
            string input;
            string[] lineOfNumbers;
            TextReader iReader = new StreamReader(s);
            for (int i = 0; i < this.N; i++)
            {
                input = iReader.ReadLine();
                lineOfNumbers = input.Split(' ');
                for (int j = 0; j < this.N; j++)
                {
                    matrix[i][j] = Convert.ToDouble(lineOfNumbers[j]);
                }
            }
            iReader.Close();
        }


    }
}
